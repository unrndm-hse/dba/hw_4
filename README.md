Homework 4
==========

Task 1
------
Give relational algebra expressions for the intensional views r and s defined by the following
Datalog program in function of the extensional relations t, u, and v:
```
r(X,Y) :- s(X,X), t(Y), not(s(Y,Y)).
s(X,Y) :- u(V,W), v(W,X), v(W,Y), not(X=Y).
```

### Solution:
```
r = Π [s1, s2] (σ [x==s1==s2] ( s ) ⋉ σ [y==t1] ( t )) - σ [y==s1==s2] ( s )
s = Π [u1, s2] (σ [u1==V, u2==W] ( u ) ⋉ σ [v1==W, v2==X, x!=Y] ( v ) ⋉ σ [v1==W, v2==Y, x!=Y] ( v ))))
```

Task 2
------
Consider the following Datalog program:
```
bi(X,Y) :- g(X,Y).
bi(Y,X) :- g(X,Y).
even(X,Y) :- bi(X,Z), bi(Z,Y).
even(X,Y) :- bi(X,U),bi(U,V),even(V,Y).
```
g contains the following facts: {(a, b), (b, c), (c, d), (d, e), (e, f ), (f, g)}

 | g(X, Y) ||
|-----|-----|
|  a  |  b  |
|  b  |  c  |
|  c  |  d  |
|  d  |  e  |
|  e  |  f  |
|  f  |  g  |

- (a) Show the subsequent steps of the naive recursion for computing the intensional relation even.
- (b) Show also the semi-naive implementation of the recursion. Give, e.g., relational algebra expressions that compute the content of the even and bi relations, as well as the delta-relations in the next step of the recursion based on the cont

### Solution:
 a. 

copy g:

| bi(X, Y) ||
|-----|-----|
|  a  |  b  |
|  b  |  c  |
|  c  |  d  |
|  d  |  e  |
|  e  |  f  |
|  f  |  g  |

add reversed:

| bi(X, Y) ||
|-----|-----|
|  b  |  a  |
|  c  |  b  |
|  d  |  c  |
|  e  |  d  |
|  f  |  e  |
|  g  |  f  |

even(X,Y) is bi left join where 2 close columns are the same, but since they're reversed it'll be the same in both columns and reverse from bi

even(X, Y)

|  X  |  Z  |  Z  |  Y  |
|-----|-----|-----|-----|
|  a  |  b  |  b  |  c  |
|  a  |  b  |  b  |  a  |
|  b  |  c  |  c  |  d  |
|  b  |  c  |  c  |  b  |
|  c  |  d  |  d  |  e  |
|  c  |  d  |  d  |  c  |
|  d  |  e  |  e  |  f  |
|  d  |  e  |  e  |  d  |
|  e  |  f  |  f  |  g  |
|  e  |  f  |  f  |  e  |
|  f  |  g  |  g  |  f  |
|  b  |  a  |  a  |  b  |
|  c  |  b  |  b  |  c  |
|  c  |  b  |  b  |  a  |
|  d  |  c  |  c  |  d  |
|  d  |  c  |  c  |  b  |
|  e  |  d  |  d  |  e  |
|  e  |  d  |  d  |  c  |
|  f  |  e  |  e  |  f  |
|  f  |  e  |  e  |  d  |
|  g  |  f  |  f  |  g  |
|  g  |  f  |  f  |  e  |

| even(X,Y)||
|-----|-----|
|  a  |  a  |
|  a  |  c  |
|  b  |  b  |
|  b  |  d  |
|  c  |  a  |
|  c  |  c  |
|  c  |  e  |
|  d  |  b  |
|  d  |  d  |
|  d  |  f  |
|  e  |  c  |
|  e  |  e  |
|  e  |  g  |
|  f  |  d  |
|  f  |  f  |
|  g  |  e  |
|  g  |  g  |

even(X,Y) :- bi(X,U),bi(U,V),even(V,Y).
the same, but with even

even(X, Y)

|  X  |  V  |  V  |  Y  |
|-----|-----|-----|-----|
|  a  |  a  |  a  |  a  |
|  a  |  a  |  a  |  c  |
|  a  |  c  |  c  |  a  |
|  a  |  c  |  c  |  c  |
|  a  |  c  |  c  |  e  |
|  b  |  b  |  b  |  b  |
|  b  |  b  |  b  |  d  |
|  b  |  d  |  d  |  b  |
|  b  |  d  |  d  |  d  |
|  b  |  d  |  d  |  f  |
|  c  |  a  |  a  |  a  |
|  c  |  a  |  a  |  c  |
|  c  |  c  |  c  |  a  |
|  c  |  c  |  c  |  c  |
|  c  |  c  |  c  |  e  |
|  c  |  c  |  c  |  c  |
|  c  |  e  |  e  |  c  |
|  c  |  e  |  e  |  e  |
|  c  |  e  |  e  |  g  |
|  d  |  b  |  b  |  b  |
|  d  |  b  |  b  |  d  |
|  d  |  d  |  d  |  b  |
|  d  |  d  |  d  |  d  |
|  d  |  d  |  d  |  f  |
|  d  |  f  |  f  |  d  |
|  d  |  f  |  f  |  f  |
|  e  |  c  |  c  |  a  |
|  e  |  c  |  c  |  c  |
|  e  |  c  |  c  |  e  |
|  e  |  e  |  e  |  c  |
|  e  |  e  |  e  |  e  |
|  e  |  e  |  e  |  g  |
|  e  |  g  |  g  |  e  |
|  e  |  g  |  g  |  g  |
|  f  |  d  |  d  |  b  |
|  f  |  d  |  d  |  d  |
|  f  |  d  |  d  |  f  |
|  f  |  f  |  f  |  d  |
|  f  |  f  |  f  |  f  |
|  g  |  e  |  e  |  c  |
|  g  |  e  |  e  |  e  |
|  g  |  e  |  e  |  g  |
|  g  |  g  |  g  |  e  |
|  g  |  g  |  g  |  g  |
 

|  X  |  Y  |
|-----|-----|
|  a  |  a  |
|  a  |  c  |
|  a  |  e  |
|  b  |  b  |
|  b  |  d  |
|  b  |  f  |
|  c  |  a  |
|  c  |  c  |
|  c  |  e  |
|  c  |  g  |
|  d  |  b  |
|  d  |  d  |
|  d  |  f  |
|  e  |  a  |
|  e  |  c  |
|  e  |  e  |
|  e  |  g  |
|  f  |  b  |
|  f  |  d  |
|  f  |  f  |
|  g  |  c  |
|  g  |  e  |
|  g  |  g  |

 b. idk

Task 3
------
Consider the following program:
```
reach(X,Y) :- g(X,Y).
reach(X,Y) :- g(X,Z), reach(Z,Y).
```
g contains the following facts: {(a, b), (b, c), (c, d), (d, e), (f, g), (g, h), (h, i), (b, i)}

 | g(X, Y) ||
|-----|-----|
|  a  |  b  |
|  b  |  c  |
|  c  |  d  |
|  d  |  e  |
|  f  |  g  |
|  g  |  h  |
|  h  |  i  |

(a) Illustrate that even the semi-naive recursion is not very efficient when computing the answer to the query ?reach(c,Y), asking for all nodes reachable from c.

(b) Evaluate the query ?reach(c,Y) using top-down (goal-driven) evaluation.

(c) Consider the following program in which a “magic set” is introduced. Evaluate now again the query `?reach(c,Y)`
```
m(c).
m(Z) :- m(X),g(X,Z).
reach(X,Y) :- m(X), g(X,Y).
reach(X,Y) :- m(X), g(X,Z), reach(Z,Y).
```
(d) * Show how to rewrite the program if the query were ?reach(c,e).

(e) ** What if the query is ?reach(X,e) ?

### Solution:
|reach(X,Y)||
|-----|-----|
|  a  |  b  |
|  a  |  c  |
|  a  |  d  |
|  b  |  c  |
|  b  |  d  |
|  c  |  d  |
|  c  |  e  |
|  d  |  e  |
|  f  |  g  |
|  f  |  h  |
|  f  |  i  |
|  g  |  h  |
|  g  |  i  |
|  h  |  i  |


 a. will need to recurse at least 4 times to find 4 relationships

 b. (c, d), (c, e)
 
 c. m: c, d
    reach: (c, d), (d, e), (c, e)
    res: d, e (?)

 d. idk

 e. idk
